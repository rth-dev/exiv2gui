'''
###############################################################################################
#
#    exiv2gui    
#
###############################################################################################
#
#    Author:         rth.
#
#    Copyright:      © 2018
#
#    License:        General Public License, Version 3 or newer
#                    http://www.gnu.org/licenses/gpl-3.0.html
#    
#
#    Use the software at your own risk
#
###############################################################################################
'''

import subprocess


''' checks whether Exiv2 binary can be found installed '''
def exifv2_exists():
    c = subprocess.run("which exiv2", shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    if c.returncode != 0:
        return False
    return True



''' returns the list index of a key '''
def get_keylist_index(keylist, key):
    for idx in range( 1, keylist.__len__() ):
        if key == keylist[idx].get('key'): 
            return keylist[idx].get('idx')
    raise Exception("get_keylist_type() could not find index for \"" + key + "\"")


''' returns the tag value of a key '''
def get_keylist_tag(keylist, key):
    for idx in range( 1, keylist.__len__() ):
        if key == keylist[idx].get('key'): 
            return keylist[idx].get('tag')
    raise Exception("get_keylist_type() could not find tag for \"" + key + "\"")


''' returns the image file directory (IFD) index of a key '''
def get_keylist_ifd(keylist, key):
    for idx in range( 1, keylist.__len__() ):
        if key == keylist[idx].get('key'): 
            return keylist[idx].get('ifd')
    raise Exception("get_keylist_type() could not find IFD for \"" + key + "\"")


''' returns the type of a key '''
def get_keylist_type(keylist, key):
    for idx in range( 1, keylist.__len__() ):
        if key == keylist[idx].get('key'): 
            return keylist[idx].get('type')
    raise Exception("get_keylist_type() could not find type for \"" + key + "\"")


''' returns the description of a key '''
def get_keylist_descr(keylist, key):
    for idx in range( 1, keylist.__len__() ):
        if key == keylist[idx].get('key'): 
            return keylist[idx].get('descr')
    raise Exception("get_keylist_type() could not find description for \"" + key + "\"")




''' reads one key entry from file ,   
#     mode = True  :  reads interpreted/translated values
#     mode = False :  reads plain data value values
'''
def read_key(file, key, interpreted):
    mode = "v"
    if interpreted:
        mode = "t"
    cmd = "exiv2 -PE" + mode + " -K \"" + str(key) + "\" \"" + str(file) + "\""
    c = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    if c.returncode != 0:
        print("Command: " + cmd)
        #raise Exception("read_key(): exifv2 returns with error: " + str(c.returncode))
    return c.stdout.rstrip('\n') 




''' writes one key entry to file '''   
def write_key(file, key, ktype, value):
    val = ""
    if value != "":
        val = " " + str(ktype) + " " + str(value)    
    cmd = "exiv2 -M 'set " + str(key) + val + "' \"" + str(file) + "\""
    c = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    if c.returncode != 0:
        print("Command: " + cmd)
        #raise Exception("write_key(): exifv2 returns with error: " + str(c.returncode))




''' deletes one key from file '''   
def delete_key(file, key):
    cmd = "exiv2 -M 'del " + str(key) + "' \"" + str(file) + "\""
    c = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    if c.returncode != 0:
        print("Command: " + cmd)
        #raise Exception("delete_key(): exifv2 returns with error: " + str(c.returncode))



