#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
###############################################################################################
#
#    exiv2gui
#
###############################################################################################
#
#    Author:         rth.
#
#    Copyright:      © 2020
#
#    License:        General Public License, Version 3 or newer
#                    http://www.gnu.org/licenses/gpl-3.0.html
#
#
#    Use the software at your own risk
#
###############################################################################################
#
#    *** USEFULL LINKS  ****
#    - exiv2 manpage                >  www.exiv2.org/manpage.html
#    - glade GUI builder            >  glade.gnome.org
#    - Python und Glade Tutorial    >  www.florian-diesch.de/doc/python-und-glade/online
#    - Standard Exif Tags           >  www.exiv2.org/tags.html
#    - GObject Reference (GTK+3)    >  lazka.github.io/pgi-docs
#
###############################################################################################
'''

import os, sys, traceback, datetime

''' GTK library version '''
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk


''' exiv2gui imports '''
from func import exiv2wrapper
from func.exifkeys import exifStdList

runpath = os.path.abspath(os.path.dirname(sys.argv[0]))

prog_name      = "exiv2gui"
prog_version   = "1.2beta"

prog_comment   = "This is an GUI for the command line utility Exiv2,\n used for manipulate the metadata of images"
prog_webside   = "https://dev-used.de"
prog_copyright = "Copyright © 2020 rth."
prog_author    = ["rth."]
prog_license   = "\nGNU General Public License, Version 3\n\nhttp://www.gnu.org/licenses/gpl-3.0.html"




class Exiv2GUI (object):

    __interpreted_data = True;


    def __init__(self):

        ''' check whether exiv2 is available *** '''
        if not exiv2wrapper.exifv2_exists():
            print("\n*** stop running: it seems that exiv2 is not installed ***")
            exit()


        ''' load GUI frontend '''
        self.builder = Gtk.Builder()
        self.builder.add_from_file(runpath + "/" + "exiv2gui.glade")
        self.builder.connect_signals(self)

        ''' load images '''
        logo = Gtk.Image().new_from_file(runpath + "/images/" + "logo.png").get_pixbuf()

        ''' place logo in mainwindow '''
        self.builder.get_object("main_win_logo").set_from_pixbuf(logo)

        ''' configure about dialog '''
        self.builder.get_object("aboutdialog").set_transient_for(self.builder.get_object("main_window"))
        self.builder.get_object("aboutdialog").set_program_name(prog_name)
        self.builder.get_object("aboutdialog").set_version(prog_version)
        self.builder.get_object("aboutdialog").set_comments(prog_comment)
        #self.builder.get_object("aboutdialog").set_authors(prog_author)
        #self.builder.get_object("aboutdialog").set_license(prog_license)
        self.builder.get_object("aboutdialog").set_website(prog_webside)
        self.builder.get_object("aboutdialog").set_copyright(prog_copyright)
        self.builder.get_object("aboutdialog").set_logo(logo)


        ''' ---- FAR TO SLOW HERE ------------------------------------------------------------------------ '''
        ''' add standard keylist to all comboboxes '''
        #for boxId in range(1,21):
        #    for key in exifStdList:
        #        self.builder.get_object("comboboxtext"+str(boxId)).append_text(key.get('key'))
        #        self.builder.get_object("comboboxtext"+str(boxId)).set_tooltip_text(key.get('descr'))


        ''' set default keys '''
        #self.__set_combobox_default_key( 1, 'Exif.Photo.DateTimeDigitized')
        #self.__set_combobox_default_key( 2, 'Exif.Photo.DateTimeOriginal')
        #self.__set_combobox_default_key( 3, 'Exif.Image.DateTime')
        #self.__set_combobox_default_key( 4, 'Exif.Image.Make')
        #self.__set_combobox_default_key( 5, 'Exif.Image.Model')
        #self.__set_combobox_default_key( 6, 'Exif.Image.CameraSerialNumber')
        #self.__set_combobox_default_key( 7, 'Exif.Photo.LensMake')
        #self.__set_combobox_default_key( 8, 'Exif.Photo.LensModel')
        #self.__set_combobox_default_key( 9, 'Exif.Photo.FocalLength')
        #self.__set_combobox_default_key(10, 'Exif.Photo.FocalLengthIn35mmFilm')
        #self.__set_combobox_default_key(11, 'Exif.Photo.MaxApertureValue')
        #self.__set_combobox_default_key(12, 'Exif.Photo.FNumber')
        #self.__set_combobox_default_key(13, 'Exif.Photo.ExposureTime')
        #self.__set_combobox_default_key(14, 'Exif.Photo.ISOSpeedRatings')
        #self.__set_combobox_default_key(15, 'Exif.Photo.ExposureProgram')
        #self.__set_combobox_default_key(16, 'Exif.Photo.ColorSpace')
        #self.__set_combobox_default_key(17, 'Exif.Image.ImageDescription')
        #self.__set_combobox_default_key(18, 'Exif.Photo.UserComment')
        #self.__set_combobox_default_key(19, 'Exif.Image.Artist')
        #self.__set_combobox_default_key(20, 'Exif.Image.Copyright')
        #self.__set_combobox_default_key(21, 'Exif.Image.Software')
        #Xmp.dc.title
        #Exif.Photo.FileSource
        #Exif.Photo.BodySerialNumber

        ''' set default settings '''
        self.builder.get_object("checkbutton_activate_all_keys").set_active(True)
        self.builder.get_object("checkbutton_interpreted_data").set_active(True)
        self.builder.get_object("filechooser").set_current_folder( str(os.environ.get('HOME')) )
        self.builder.get_object("checkbutton_update_filechooser").set_active(True)
        

        ''' set timestamp '''
        now = datetime.datetime.now()
        self.builder.get_object("label_date_now").set_text(now.strftime('%Y:%m:%d %H:%M:%S'))



    def run(self):
        self.builder.get_object('main_window').connect("destroy", Gtk.main_quit)
        self.builder.get_object('main_window').show_all()
        Gtk.main()



    '''___________________________________________________________________
    **    signal handler
    '''

    def on_key_release_event(self, window, event):
        ev = Gdk.keyval_name(event.keyval)
        if ev == 'F1':
            self.__update_exif_data_all(False)
        if ev == 'F2':
            if not self.builder.get_object("checkbutton_interpreted_data").get_active():
                self.__update_exif_data_all(True)
            else:
                print("save not allowed if interpreted is enabled")
        if ev == 'F3':
            print("-- not yet a good idea! --")
        if ev == 'F4':
            Gtk.main_quit()


    def on_main_window_destroy(self, *args):
        Gtk.main_quit()


    def on_button_exit_clicked(self, *args):
        Gtk.main_quit()


    def on_button_load_clicked(self, *args):
        self.__update_exif_data_all(False)


    def on_button_save_clicked(self, *args):
        self.__update_exif_data_all(True)


    def on_button_delete_clicked(self, *arg):
        try:
            filepath = self.builder.get_object("filechooser").get_filename()
            for cnt in range(1, 21):
                state = self.builder.get_object("checkbutton" + str(cnt)).get_active()
                if state:
                    key = self.builder.get_object("comboboxtext" + str(cnt)). get_active_text()
                    exiv2wrapper.delete_key(filepath, key)
        except FileNotFoundError:
            pass
        except TypeError:
            pass
        except Exception as e:
            print("\n*** on_button_delete_clicked() got exception ***\n    > " + str(e) )
            traceback.print_exc()


    def on_filechooser_selection_changed(self, *args):
        if self.builder.get_object("checkbutton_update_filechooser").get_active():
            self.__update_exif_data_all(False)


    def on_checkbutton_activate_all_keys_toggled(self, *arg):
        state = self.builder.get_object("checkbutton_activate_all_keys").get_active()
        for cnt in range(1,22):
            self.builder.get_object("checkbutton" + str(cnt)).set_active(state)


    def on_checkbutton_interpreted_data_toggled(self, *arg):
        state = self.builder.get_object("checkbutton_interpreted_data").get_active()
        self.builder.get_object("button_save").set_sensitive(not state)
        self.__interpreted_data = state
        self.__update_exif_data_all(False)


    def on_imagemenuitem_about_activate(self, *arg):
        self.builder.get_object("aboutdialog").run()
        self.builder.get_object("aboutdialog").hide()


    def on_comboboxtext1_changed(self, *arg):
        self.__update_exif_data_checkbox(1, False)


    def on_comboboxtext2_changed(self, *arg):
        self.__update_exif_data_checkbox(2, False)


    def on_comboboxtext3_changed(self, *arg):
        self.__update_exif_data_checkbox(3, False)


    def on_comboboxtext4_changed(self, *arg):
        self.__update_exif_data_checkbox(4, False)


    def on_comboboxtext5_changed(self, *arg):
        self.__update_exif_data_checkbox(5, False)


    def on_comboboxtext6_changed(self, *arg):
        self.__update_exif_data_checkbox(6, False)


    def on_comboboxtext7_changed(self, *arg):
        self.__update_exif_data_checkbox(7, False)


    def on_comboboxtext8_changed(self, *arg):
        self.__update_exif_data_checkbox(8, False)


    def on_comboboxtext9_changed(self, *arg):
        self.__update_exif_data_checkbox(9, False)


    def on_comboboxtext10_changed(self, *arg):
        self.__update_exif_data_checkbox(10, False)


    def on_comboboxtext11_changed(self, *arg):
        self.__update_exif_data_checkbox(11, False)


    def on_comboboxtext12_changed(self, *arg):
        self.__update_exif_data_checkbox(12, False)


    def on_comboboxtext13_changed(self, *arg):
        self.__update_exif_data_checkbox(13, False)


    def on_comboboxtext14_changed(self, *arg):
        self.__update_exif_data_checkbox(14, False)


    def on_comboboxtext15_changed(self, *arg):
        self.__update_exif_data_checkbox(15, False)


    def on_comboboxtext16_changed(self, *arg):
        self.__update_exif_data_checkbox(16, False)


    def on_comboboxtext17_changed(self, *arg):
        self.__update_exif_data_checkbox(17, False)


    def on_comboboxtext18_changed(self, *arg):
        self.__update_exif_data_checkbox(18, False)


    def on_comboboxtext19_changed(self, *arg):
        self.__update_exif_data_checkbox(19, False)


    def on_comboboxtext20_changed(self, *arg):
        self.__update_exif_data_checkbox(20, False)


    def on_comboboxtext21_changed(self, *arg):
        self.__update_exif_data_checkbox(21, False)


    '''___________________________________________________________________
    **    class methods
    '''
    def __set_combobox_default_key(self, boxId, key):
        idx = 0
        for dic in exifStdList:
            if dic.get('key') == key:
                idx = dic.get('idx')
                break
        self.builder.get_object("comboboxtext"+str(boxId)).set_active(idx)


    def __update_exif_data_checkbox(self, boxId, write2file=False):
        file = self.builder.get_object("filechooser").get_filename()

        if file == None or os.path.isdir(file):
            #raise Exception("update_exif_data_box(): filepath error \"" + str(file) + "\"" )
            return

        if self.builder.get_object("checkbutton" + str(boxId)).get_active():
            if write2file:
                ''' write file '''
                val = self.builder.get_object("entry" + str(boxId)).get_text()
                key = self.builder.get_object("comboboxtext"+str(boxId)).get_active_text()
                ktype = exiv2wrapper.get_keylist_type(exifStdList, key)
                exiv2wrapper.write_key(file, key, ktype, val)
            else:
                ''' read file '''
                key = self.builder.get_object("comboboxtext"+str(boxId)).get_active_text()
                val = exiv2wrapper.read_key(file, key, self.__interpreted_data)
                if val == None:
                    val = ""
                self.builder.get_object("entry" + str(boxId)).set_text(val)



    def __update_exif_data_all(self, write2file=False):
        try:
            for cnt in range(1,22):
                self.__update_exif_data_checkbox(cnt, write2file)
        #except FileNotFoundError:
        #    pass
        #except TypeError:
        #    pass
        except Exception:
            print("\n*** update_exif_data() got exception ***")
            traceback.print_exc()



'''___________________________________________________________________
**    start the engine ...
'''
if __name__ == '__main__':
    Exiv2GUI().run()





