# exiv2gui
A front-end for the command line utility <a href="http://exiv2.org/">Exiv2</a>

## Development
The program is written in <a href="https://www.python.org/">Python</a>. The GUI is designed with <a href="https://glade.gnome.org/">Glade</a>.

The Metadata reference tables can be found online <a href="https://exiv2.org/metadata.html">here</a>.
